package org.intfact

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.work.ListenableWorker
import androidx.work.testing.TestListenableWorkerBuilder
import androidx.work.workDataOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TestCalcWorker {
    @Test
    fun test() {
        val appCtx: Context = ApplicationProvider.getApplicationContext()
        val worker = TestListenableWorkerBuilder<CalcWorker>(appCtx)
            .setInputData(workDataOf(CalcWorker.KEY_INT_NUM to 10))
            .build()

        runBlocking {
            val rez = worker.startWork().get()
            val exp = "10 = 2 * 5 ".trim()
            val er: ListenableWorker.Result.Success = rez as ListenableWorker.Result.Success
            val act = er.outputData.getString(CalcWorker.KEY_OUTPUT_STR)?.trim()
            Assert.assertTrue(exp.compareTo(act!!) == 0)
        }

    }
}