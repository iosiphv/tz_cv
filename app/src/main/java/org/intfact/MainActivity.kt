package org.intfact

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import org.intfact.databinding.ActivityMainBinding
import java.lang.Integer.parseInt

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bind = DataBindingUtil
            .setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        var num = 0

        val observerTextResult = Observer<String> {
            run {
                bind.textView.text = it
                bind.editTextNumber.isEnabled = true
            }
        }

        val vm by viewModels<MainViewModel>()
        vm.getData().observe(this, observerTextResult)

        bind.editTextNumber
            .addTextChangedListener(afterTextChanged = {
                try {
                    if (it?.isNotBlank() == true) num = parseInt("$it")
                } catch (ex: NumberFormatException) {
                    Log.e("INT", ex.localizedMessage)
                    num = 0
                    it?.clear()
                    bind.textView.text = "need number < ${Int.MAX_VALUE} !!!"
                }
            })

        bind.button.setOnClickListener {
            bind.editTextNumber.isEnabled = false
            bind.textView.text = "Working..."
            vm.find(num)
        }
    }
}
