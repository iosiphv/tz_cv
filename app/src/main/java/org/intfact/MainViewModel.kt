package org.intfact

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager

open class MainViewModel(app: Application) : AndroidViewModel(app) {
    private val liveData: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    private val workMan = WorkManager.getInstance(app)

    open fun getData(): LiveData<String> {
        return liveData
    }

    fun find(num: Int) {
        val data = Data.Builder()
            .putInt(CalcWorker.KEY_INT_NUM, num)
            .build()

        val req = OneTimeWorkRequestBuilder<CalcWorker>()
            .setInputData(data)
            .build()
        workMan.enqueue(req)
        workMan
            .getWorkInfoByIdLiveData(req.id)
            .observeForever {
                if (it.state.isFinished) {
                    val str = it.outputData.getString(CalcWorker.KEY_OUTPUT_STR)
                    liveData.postValue(str)
                }
            }
    }
}