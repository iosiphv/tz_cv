package org.intfact

import android.content.Context
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.calcfactorizz.IntFactoriz

class CalcWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    companion object {
        const val KEY_INT_NUM = "NUM"
        const val KEY_OUTPUT_STR = "STR"
    }

    override fun doWork(): Result {
        val num = inputData.getInt(KEY_INT_NUM, 0)
        val fzzz = IntFactoriz()
        val str = "$num = " +
                fzzz.factor(num)
                    .map(Int::toString)
                    .reduce { t, s -> "$t * $s " }
        val out = Data.Builder().putString(KEY_OUTPUT_STR, str).build()
        return Result.success(out)
    }
}