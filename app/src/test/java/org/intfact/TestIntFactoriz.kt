package org.intfact

import com.calcfactorizz.IntFactoriz
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class TestIntFactoriz {
    private var nf: IntFactoriz? = null
    @Before
    fun setUp() {
        nf = IntFactoriz()
    }

    @Test
    fun factorZero() {
        val act = nf!!.factor(0)
        Assert.assertArrayEquals(arrayOf(0), act)
    }

    @Test
    fun factorOne() {
        val act = nf!!.factor(1)
        Assert.assertArrayEquals(arrayOf(1), act)
    }

    @Test
    fun factorTwo() {
        val act = nf!!.factor(2)
        Assert.assertArrayEquals(arrayOf(2), act)
    }
}