package org.intfact

import com.calcfactorizz.IntFactoriz
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters
import java.io.Serializable

@RunWith(Parameterized::class)
class TestIntFactorizParametrizedEquals(private var expDivs: Array<Int>, private var num: Int) {
    private var nf: IntFactoriz? = null

    companion object {
        @JvmStatic
        @Parameters
        fun data(): List<Array<Serializable>> {
            return listOf(
                arrayOf(arrayOf(3), 3),
                arrayOf(arrayOf(5), 5),
                arrayOf(arrayOf(7), 7),
                arrayOf(arrayOf(11), 11),
                arrayOf(arrayOf(3, 3), 9),
                arrayOf(arrayOf(3, 3, 5), 45),
                arrayOf(arrayOf(599, 761), 455839),
                arrayOf(arrayOf(3851, 5531), 21299881),
                arrayOf(arrayOf(13, 23), 299),
                arrayOf(arrayOf(4451, 4969), 22117019)
            ).toList()
        }
    }

    @Before
    fun setUp() {
        nf = IntFactoriz()
    }

    @Test
    fun findDividers() {
        val actArray = nf!!.factor(num)
        val expArray = expDivs
        Assert.assertArrayEquals(expArray, actArray)
    }
}