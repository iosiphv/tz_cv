package com.calcfactorizz

import kotlin.math.abs
import kotlin.math.roundToLong
import kotlin.math.sqrt

/*
*
* Нужно написать функцию на котлине, которая будет факторизировать переменную x и возвращать массив из простых чисел:
*
* fun factor(x: Int): Array {
*  //реализация алгоритма
* }
*
* Например,
* для x = 45 функция должна вернуть массив [3, 3, 5]. 3 и 5 это простые числа. Eсли 3 умножить на 3 и на 5, то будет 45.
* для x = 3 функция вернет [3]
* для x = 210 вернет [2, 3, 5, 7]
*
* */
class IntFactoriz {
    fun factor(x: Int): Array<Int> {
        var quotient = if (x < 0) abs(x) else x
        val rez = ArrayList<Int>(5)
        var nextDivider = 1

        while (quotient > 1) {
            nextDivider = findNextForInt(quotient)
            rez.add(nextDivider)
            quotient /= nextDivider
        }
        if (rez.isEmpty()) rez.add(quotient)
        return rez.toTypedArray()
    }

    private fun findNextForInt(num: Int): Int {
        val n = sqrt(num.toDouble()).roundToLong()
        return (2..n).find { i -> num.rem(i).compareTo(0) == 0 }?.toInt() ?: num
    }
}