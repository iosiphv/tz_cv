package com.calcfactorizz

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    TestIntFactorizParametrizedEquals::class,
    TestIntFactoriz::class
)
class TestSuite