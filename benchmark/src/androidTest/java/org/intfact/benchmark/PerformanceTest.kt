package org.intfact.benchmark

import androidx.benchmark.junit4.BenchmarkRule
import androidx.benchmark.junit4.measureRepeated
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.calcfactorizz.IntFactoriz
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PerformanceTest {
    @get:Rule
    val benchmarkRule = BenchmarkRule()

    @Test
    fun benchIntFactorizz() = benchmarkRule.measureRepeated {
        val intFactorizzz = IntFactoriz()
        intFactorizzz.factor(Int.MAX_VALUE - 1)
    }

}